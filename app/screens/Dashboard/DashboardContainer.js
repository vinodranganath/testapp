import React, {useEffect, useState} from 'react';
import DashboardView from './DashboardView';
import * as PropType from 'prop-types';

const DashboardContainer = (props) => {
  const {navigation} = props;
  const [count, setCount] = useState(0);
  const navigationList = [
    { pageName: 'First', screenName: 'Home' },
    { pageName: 'Third', screenName: 'Page B' },
  ];

  useEffect(() => {
    console.log('Dashboard');
  }, []);

  useEffect(() => {
    console.log('count', count);
  }, [count]);

  const incrementCount = () => setCount(count + 1);

  const decrementCount = () => setCount(count - 1);

  return (
    <DashboardView
      count={count}
      increment={incrementCount}
      decrement={decrementCount}
      navigationList={navigationList}
      navigateTo={(pageName, screenName) => navigation.navigate( pageName, {screen: screenName})}
    />
  );
};

DashboardContainer.propTypes = {
  navigation: PropType.shape({
    navigate: PropType.func.isRequired,
  }).isRequired,
};

DashboardContainer.defaultProps = {
  navigation: {},
};

export default DashboardContainer;
