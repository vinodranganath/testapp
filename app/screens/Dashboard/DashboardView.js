import React from 'react';
import {CounterControl, DisplayTextContent, NavigationButtons} from '../components';
import {ContentWrapper} from '../styles';
import * as PropType from 'prop-types';

const DashboardView = (props) => {
  const {count, increment, decrement, navigationList, navigateTo} = props;

  return (
    <ContentWrapper>
      <DisplayTextContent text="Dashboard (Tab)" />
      <CounterControl count={count} increment={increment} decrement={decrement} />
      <NavigationButtons navigationList={navigationList} navigateTo={navigateTo} />
    </ContentWrapper>
  );
};

DashboardView.propTypes = {
  count: PropType.number,
  increment: PropType.func,
  decrement: PropType.func,
  navigationList: PropType.array,
  navigateTo: PropType.func,
};

DashboardView.defaultProps = {
  count: 0,
  increment: () => {},
  decrement: () => {},
  navigationList: [],
  navigateTo: () => {},
};

export default DashboardView;
