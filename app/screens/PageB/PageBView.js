import React from 'react';
import {CounterControl, DisplayTextContent, NavigationButtons} from '../components';
import {ContentWrapper} from '../styles';
import * as PropType from 'prop-types';

const PageBView = (props) => {
  const {count, increment, decrement, navigationList, navigateTo} = props;

  return (
    <ContentWrapper>
      <DisplayTextContent text="Page B (Stack)" />
      <CounterControl count={count} increment={increment} decrement={decrement} />
      <NavigationButtons navigationList={navigationList} navigateTo={navigateTo} />
    </ContentWrapper>
  );
};

PageBView.propTypes = {
  count: PropType.number,
  increment: PropType.func,
  decrement: PropType.func,
  navigationList: PropType.array,
  navigateTo: PropType.func,
};

PageBView.defaultProps = {
  count: 0,
  increment: () => {},
  decrement: () => {},
  navigationList: [],
  navigateTo: () => {},
};

export default PageBView;
