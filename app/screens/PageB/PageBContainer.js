import React, {useEffect, useState} from 'react';
import PageBView from './PageBView';
import * as PropType from 'prop-types';

const PageBContainer = (props) => {
  const {navigation} = props;
  const [count, setCount] = useState(0);
  const navigationList = [
    { pageName: 'First', screenName: 'Home' },
    { pageName: 'First', screenName: 'Dashboard' },
    { pageName: 'Second', screenName: 'Page A' },
    { pageName: 'AppRating', screenName: 'App Rating' },
  ];

  useEffect(() => {
    console.log('Page B');
  }, []);

  useEffect(() => {
    console.log('count', count);
  }, [count]);

  const incrementCount = () => setCount(count + 1);

  const decrementCount = () => setCount(count - 1);

  return (
    <PageBView
      count={count}
      increment={incrementCount}
      decrement={decrementCount}
      navigationList={navigationList}
      navigateTo={(pageName, screenName) => navigation.navigate( pageName, {screen: screenName})}
    />
  );
};

PageBContainer.propTypes = {
  navigation: PropType.shape({
    navigate: PropType.func.isRequired,
  }).isRequired,
};

PageBContainer.defaultProps = {
  navigation: {},
};

export default PageBContainer;
