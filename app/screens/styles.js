import styled from 'styled-components';
import {Button, Colors, IconButton, Text} from 'react-native-paper';
import {Animated} from 'react-native';

export const ContentWrapper = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const CounterWrapper = styled.View`
	width: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const CountText = styled(Text)`
  width: 200px;
  text-align: center;
  font-size: 100px;
  font-weight: 100;
  color: ${Colors.tealA700};
`;

export const ActionButton = styled(IconButton)`
  background-color: ${Colors.grey800};
`;

export const NavigationWrapper = styled.View`
  justify-content: space-evenly;
  align-items: center;
`;

export const NavigationButton = styled(Button)`
	margin: 10px 0px;
`;

export const DisplayText = styled(Text)`
	width: 100%;
	color: ${Colors.amber800};
	font-size: 30px;
	font-weight: 100;
	text-align: center;
`;

export const GestureWrapper = styled(Animated.View)`
	margin-top: 30px;
`;

export const GestureTargetContainer = styled.View`
	width: 300px;
	height: 300px;
	border: solid 1px #444;
	justify-content: center;
  align-items: center;
`;

export const GestureTarget = styled.View`
	width: 150px;
	height: 150px;
	border: solid 1px #444;
	justify-content: center;
  align-items: center;
`;
