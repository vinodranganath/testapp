import React from 'react';
import {DisplayTextContent} from '../components';
import {ContentWrapper, NavigationButton} from '../styles';

const AppRatingView = (props) => {
	const {showNowCallback} = props;

	return (
		<ContentWrapper>
			<DisplayTextContent text="App Rating" />
			<NavigationButton
				mode="contained"
				onPress={() => showNowCallback()}
			>
				Show Now
			</NavigationButton>
		</ContentWrapper>
	);
};

export default AppRatingView;
