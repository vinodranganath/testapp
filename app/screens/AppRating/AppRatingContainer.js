import React, {useEffect, useState} from 'react';
import {NativeModules} from 'react-native';
import AppRatingView from './AppRatingView';

const AppRatingContainer = () => {
	const {AppRatingModule} = NativeModules;
	const [userRating, setUserRating] = useState({});

	useEffect(() => console.log('AppRatingModule: userRating', userRating), [userRating]);

	const showRatingDialog = () => {
		AppRatingModule.showRatingDialog(
			userRating => setUserRating(userRating),
			() => console.log('AppRatingModule: popup not shown'),
		);
	};

	return (
		<AppRatingView showNowCallback={() => showRatingDialog()} />
	);
};

export default AppRatingContainer;
