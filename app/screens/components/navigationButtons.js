import React from 'react';
import * as PropType from 'prop-types';
import {NavigationButton, NavigationWrapper} from '../styles';

const NavigationButtons = (props) => {
	const {navigationList, navigateTo} = props;

	return (
		<NavigationWrapper>
			{
				navigationList.map((navItem) => {
					const {pageName, screenName} = navItem;
					return (
						<NavigationButton
							icon="arrow-right"
							mode="contained"
							contentStyle={{flexDirection: 'row-reverse'}}
							onPress={() => navigateTo(pageName, screenName)}
							key={screenName}
						>
							{screenName}
						</NavigationButton>
					);
				})
			}
		</NavigationWrapper>
	);
}

NavigationButtons.propTypes = {
	navigationList: PropType.arrayOf(
		PropType.shape({
			pageName: PropType.string,
			screenName: PropType.string,
		})
	),
	navigateTo: PropType.func,
};

NavigationButtons.defaultProps = {
	navigationList: [],
	navigateTo: () => {},
};

export default NavigationButtons;
