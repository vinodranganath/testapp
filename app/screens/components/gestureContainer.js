import React, {useEffect, useRef} from 'react';
import {GestureWrapper, GestureTargetContainer, GestureTarget} from '../styles';
import {DisplayTextContent} from './index';
import {PinchGestureHandler, State} from 'react-native-gesture-handler';
import {Animated, useWindowDimensions} from 'react-native';

const GestureContainer = () => {
	const scale = useRef(new Animated.Value(1));
	const translateX = useRef(new Animated.Value(0));
	const translateY = useRef(new Animated.Value(0));
	const dimensions = useWindowDimensions();

	useEffect(() => console.log('=========>width', dimensions.width, '\n=========>height', dimensions.height), [dimensions]);

	const handlePinchEvent = Animated.event(
		[
			{nativeEvent: {scale: scale.current}}
		],
		{
			useNativeDriver: true,
			listener: ({nativeEvent}) => {
				console.log('=========>pinchEvent', nativeEvent);
				translateX.current = (nativeEvent.focalX / dimensions.width) * 100;
				translateY.current = (nativeEvent.focalY / dimensions.height) * 100;
				console.log('=========>X', translateX.current, '\n=========>Y', translateY.current);
			},
		}
	);

	const handlePinchStateChang = ({nativeEvent}) => {
		if (nativeEvent.oldState === State.ACTIVE) scale.current = nativeEvent.scale;
	};

	return (
		<PinchGestureHandler onGestureEvent={handlePinchEvent} onHandlerStateChange={handlePinchStateChang}>
			<GestureWrapper
				style={[{transform: [
					{perspective: 200},
					{scale: scale.current}
				]}]}>
				<GestureTargetContainer>
					<GestureTarget>
						<DisplayTextContent text="Test Box" />
					</GestureTarget>
				</GestureTargetContainer>
			</GestureWrapper>
		</PinchGestureHandler>
	);
}

export default GestureContainer;
