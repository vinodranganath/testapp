import React from 'react';
import {ActionButton, CounterWrapper, CountText} from '../styles';
import {Colors} from 'react-native-paper';
import * as PropType from 'prop-types';

const CounterControl = (props) => {
	const {count, increment, decrement} = props;

	return (
		<CounterWrapper>
			<ActionButton
				icon="minus"
				color={Colors.blue500}
				size={30}
				onPress={() => decrement()}
			/>
			<CountText>{count}</CountText>
			<ActionButton
				icon="plus"
				color={Colors.blue500}
				size={30}
				onPress={() => increment()}
			/>
		</CounterWrapper>
	);
}

CounterControl.propTypes = {
	count: PropType.number,
	increment: PropType.func,
	decrement: PropType.func,
};

CounterControl.defaultProps = {
	count: 0,
	increment: () => {},
	decrement: () => {},
};

export default CounterControl;
