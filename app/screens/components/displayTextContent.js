import React from 'react';
import * as PropType from 'prop-types';
import {DisplayText} from '../styles';

const DisplayTextContent = (props) => {
	const {text} = props;

	return (
		<DisplayText>{text}</DisplayText>
	);
}

DisplayTextContent.propTypes = {
	text: PropType.string,
};

DisplayTextContent.defaultProps = {
	text: "",
};

export default DisplayTextContent;
