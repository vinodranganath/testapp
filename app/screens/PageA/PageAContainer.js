import React, {useEffect, useState} from 'react';
import PageAView from './PageAView';
import * as PropType from 'prop-types';

const PageAContainer = (props) => {
  const {navigation} = props;
  const [count, setCount] = useState(0);
  const navigationList = [
    { pageName: 'Third', screenName: 'Page B' },
    { pageName: 'AppRating', screenName: 'App Rating' },
  ];

  useEffect(() => {
    console.log('Page A');
  }, []);

  useEffect(() => {
    console.log('count', count);
  }, [count]);

  const incrementCount = () => setCount(count + 1);

  const decrementCount = () => setCount(count - 1);

  return (
    <PageAView
      count={count}
      increment={incrementCount}
      decrement={decrementCount}
      navigationList={navigationList}
      navigateTo={(pageName, screenName) => navigation.navigate( pageName, {screen: screenName})}
    />
  );
};

PageAContainer.propTypes = {
  navigation: PropType.shape({
    navigate: PropType.func.isRequired,
  }).isRequired,
};

PageAContainer.defaultProps = {
  navigation: {},
};

export default PageAContainer;
