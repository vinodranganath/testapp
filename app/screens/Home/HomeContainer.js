import React, {useEffect, useState} from 'react';
import HomeView from './HomeView';
import * as PropType from 'prop-types';

const HomeContainer = (props) => {
  const {navigation} = props;
  const [count, setCount] = useState(0);
  const navigationList = [
    { pageName: 'First', screenName: 'Dashboard' },
    { pageName: 'Third', screenName: 'Page B' },
    { pageName: 'AppRating', screenName: 'App Rating' },
  ];

  useEffect(() => {
    console.log('Home');
  }, []);

  useEffect(() => {
    console.log('count', count);
  }, [count]);

  const incrementCount = () => setCount(count + 1);

  const decrementCount = () => setCount(count - 1);

  return (
    <HomeView
      count={count}
      increment={incrementCount}
      decrement={decrementCount}
      navigationList={navigationList}
      navigateTo={(pageName, screenName) => navigation.navigate( pageName, {screen: screenName})}
    />
  );
};

HomeContainer.propTypes = {
  navigation: PropType.shape({
    navigate: PropType.func.isRequired,
  }).isRequired,
};

HomeContainer.defaultProps = {
  navigation: {},
};

export default HomeContainer;
