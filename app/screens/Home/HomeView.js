import React, {useState} from 'react';
import * as PropType from 'prop-types';
import {CounterControl, DisplayTextContent, GestureContainer, NavigationButtons} from '../components';
import {ContentWrapper} from '../styles';

const HomeView = (props) => {
  const {count, increment, decrement, navigationList, navigateTo} = props;
  const [showGestureContainer] = useState(false);

  return (
    <ContentWrapper>
      <DisplayTextContent text="Home (Tab)" />
      <CounterControl count={count} increment={increment} decrement={decrement} />
      <NavigationButtons navigationList={navigationList} navigateTo={navigateTo} />
      {showGestureContainer &&  <GestureContainer />}
    </ContentWrapper>
  );
};

HomeView.propTypes = {
  count: PropType.number,
  increment: PropType.func,
  decrement: PropType.func,
  navigationList: PropType.array,
  navigateTo: PropType.func,
};

HomeView.defaultProps = {
  count: 0,
  increment: () => {},
  decrement: () => {},
  navigationList: [],
  navigateTo: () => {},
};

export default HomeView;
