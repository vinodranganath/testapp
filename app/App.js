import React, {useEffect} from 'react';
import 'react-native-gesture-handler';
import {NativeModules} from 'react-native';
import {Provider as PaperProvider} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeScreen from './screens/Home';
import DashboardScreen from './screens/Dashboard';
import PageAScreen from './screens/PageA';
import PageBScreen from './screens/PageB';
import AppRatingScreen from './screens/AppRating';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const PageAStack = createStackNavigator();
const PageBStack = createStackNavigator();
const AppRatingStack = createStackNavigator();

const App = () => {
  const {AppRatingModule} = NativeModules;

  useEffect(() => {
    AppRatingModule.initialise()
      .then(res => console.log('App:', res))
      .catch(e => console.log('AppRating: failed', e));
  }, []);

  return (
    <PaperProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="AppRating">
          <Stack.Screen name="Zeroth">
            {() => (
              <Tab.Navigator>
                <Tab.Screen name="First">
                  {() => (
                    <HomeStack.Navigator initialRouteName="Home">
                      <HomeStack.Screen
                        name="Home"
                        component={HomeScreen}
                        options={{ headerShown: false }}
                      />
                      <HomeStack.Screen
                        name="Dashboard"
                        component={DashboardScreen}
                        options={{ headerShown: false }}
                      />
                    </HomeStack.Navigator>
                  )}
                </Tab.Screen>
                <Tab.Screen name="Second">
                  {() => (
                    <PageAStack.Navigator initialRouteName="Page A">
                      <PageAStack.Screen
                        name="Page A"
                        component={PageAScreen}
                        options={{ headerShown: false }}
                      />
                    </PageAStack.Navigator>
                    )}
                </Tab.Screen>
              </Tab.Navigator>
            )}
          </Stack.Screen>
          <Stack.Screen name="Third">
            {() => (
              <PageBStack.Navigator initialRouteName="Page B">
                <PageBStack.Screen
                  name="Page B"
                  component={PageBScreen}
                  options={{ headerShown: false }}
                />
              </PageBStack.Navigator>
            )}
          </Stack.Screen>
          <Stack.Screen name="AppRating">
            {() => (
              <AppRatingStack.Navigator initialRouteName="appRating">
                <AppRatingStack.Screen
                  name="App Rating"
                  component={AppRatingScreen}
                  options={{ headerShown: false }}
                />
              </AppRatingStack.Navigator>
            )}
          </Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
};

export default App;
