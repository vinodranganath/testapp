
package com.saferoute.appRating;

import com.saferoute.R;
import androidx.appcompat.app.AppCompatActivity;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.suddenh4x.ratingdialog.AppRating;
import com.suddenh4x.ratingdialog.preferences.RatingThreshold;
import com.suddenh4x.ratingdialog.preferences.MailSettings;

public class AppRatingModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;
  private static AppRating.Builder appRating;
  private static AppRating.Builder inAppRating;
  private UserRating userRating = new UserRating();
  private Callback storedCallback;

  public AppRatingModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "AppRatingModule";
  }

  private WritableMap fetchUserRatingMap() {
    WritableMap userRating = Arguments.createMap();
    userRating.putInt("userRating", this.userRating.getUserRating());
    userRating.putString("userFeedback", this.userRating.getUserFeedback());
    return userRating;
  }

  private void ratingSubmitCallback(int userRatingValue) {
    this.userRating.setUserRating(userRatingValue);
    if (this.userRating.getUserRating() >= 4) {
        this.userRating.setUserFeedback("");
        WritableMap userRating = this.fetchUserRatingMap();
        this.storedCallback.invoke(userRating);
    }
  }

  private void feedbackSubmitCallback(String userFeedbackText) {
    this.userRating.setUserFeedback(userFeedbackText);
    WritableMap userRating = this.fetchUserRatingMap();
    this.storedCallback.invoke(userRating);
  }

  @ReactMethod
  public void initialise(Promise promise) {
    try {
        this.inAppRating = new AppRating.Builder((AppCompatActivity)getCurrentActivity())
            .useGoogleInAppReview();
        this.appRating = new AppRating.Builder((AppCompatActivity)getCurrentActivity());
        this.appRating
            // rules
            .setMinimumLaunchTimes(5)
            .setMinimumDays(7)
            .setMinimumLaunchTimesToShowAgain(5)
            .setMinimumDaysToShowAgain(10)
            .setRatingThreshold(RatingThreshold.FOUR)
            .setDebug(true)
            // Google in-app review
//             .useGoogleInAppReview()
            // custom theme
//             .setCustomTheme(R.style.AppTheme_CustomAlertDialog)
            // initial rating
            .setRateLaterButtonTextId(R.string.rate_later_button_text)
            .setTitleTextId(R.string.rating_title)
            .setMessageTextId(R.string.rating_message)
            .setConfirmButtonTextId(R.string.rating_confirm_button_text)
            .setConfirmButtonClickListener(userRating -> this.ratingSubmitCallback(Math.round(userRating)))
            .setShowOnlyFullStars(true)
            // store rating
            .setStoreRatingTitleTextId(R.string.store_rating_title)
            .setStoreRatingMessageTextId(R.string.store_rating_message)
            .setRateNowButtonTextId(R.string.store_rating_confirm_button_text)
//             .overwriteRateNowButtonClickListener(() -> this.inAppRating.showNow())
            // custom feedback
            .setUseCustomFeedback(true)
            .setFeedbackTitleTextId(R.string.custom_feedback_title)
            .setCustomFeedbackMessageTextId(R.string.custom_feedback_message)
            .setCustomFeedbackButtonTextId(R.string.custom_feedback_button_text)
            .setCustomFeedbackButtonClickListener(userFeedbackText -> this.feedbackSubmitCallback(userFeedbackText))
            .setNoFeedbackButtonTextId(R.string.no_custom_feedback_button_text)
            // mail feedback
            .setMailFeedbackMessageTextId(R.string.mail_feedback_message)
            .setMailFeedbackButtonTextId(R.string.mail_feedback_button_text);
        promise.resolve("init success");
        return;
    } catch(Exception e) {
        promise.reject(e);
        return;
    }
  }

  @ReactMethod
  public void showRatingDialog(Callback successCallBack, Callback errorCallBack) {
    try {
        boolean isShown = this.appRating.showIfMeetsConditions();
        if (isShown) {
            this.storedCallback = successCallBack;
            return;
        }
        errorCallBack.invoke();
    } catch(Exception e) {
        errorCallBack.invoke();
    }
  }
}
