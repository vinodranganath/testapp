
package com.saferoute.appRating;

public class UserRating {
    public int userRating = 0;
    public String userFeedback = "";

    UserRating() {}

    public int getUserRating() {
        return this.userRating;
    }

    public void setUserRating(int userRating) {
        this.userRating = userRating;
    }

    public String getUserFeedback() {
        return this.userFeedback;
    }

    public void setUserFeedback(String userFeedback) {
        this.userFeedback = userFeedback;
    }
}
